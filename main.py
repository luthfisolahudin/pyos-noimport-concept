product = {
    'CPU:INTEL:I5_13500F': 'Intel Core i5 13500F',
    'MEM:SAMSUNG:D4_2666_2': 'Samsung Memory',
    'GPU:NVIDIA:RTX_3050': 'Nvidia RTX 3050',
    'STG:SAMSUNG:980': 'Samsung SSD 980',
    'STG:SEAGATE:BARRACUDA': 'Seagate BarraCuda',
}

hardware = {
    'cpu': {
        'product_code': 'CPU:INTEL:I5_13500F',
        'speed': 5_000,  # In MHz
        'arch': 'x86',
        'bit': 64,
    },

    'memory_1': {
        'product_code': 'MEM:SAMSUNG:D4_2666_2',
        'version': 'DDR4',
        'speed': 2_666,  # In MHz
        'cap': 2_000,  # In MB
    },
    'memory_2': None,
    'memory_3': {
        'product_code': 'MEM:PNY:PC_4_32_4',
        'version': 'DDR4',
        'speed': 3_200,  # In MHz
        'cap': 4_000,  # In MB
    },
    'memory_4': None,

    'pcie_1': {
        'product_code': 'GPU:NVIDIA:RTX_3050',
        'type': 'GPU',
        'speed': 1_500,  # In MHz
    },
    'pcie_2': None,

    'storage_1': {
        'product_code': 'STG:SAMSUNG:980',
        'type': 'SSD_NVME',
        'speed': 7_600,  # In MB/s
        'cap': 1_000_000,  # In MB
        'contents': [
            {
                'type': 'boot',
                'basename': '#BOOTLOADER#',
                'size': 500_000,
            },
            {
                'type': 'dir',
                'basename': 'home',
                'owner': 'root',
                'group': 'root',
                'perms': {
                    'owner': {
                        'r': True,
                        'w': True,
                        'x': False,
                    },
                    'group': {
                        'r': True,
                        'w': False,
                        'x': False,
                    },
                    'other': {
                        'r': True,
                        'w': False,
                        'x': False,
                    },
                },
                'contents': [
                    {
                        'type': 'dir',
                        'basename': 'alice',
                        'owner': 'alice',
                        'group': 'alice',
                        'perms': {
                            'owner': {
                                'r': True,
                                'w': True,
                                'x': False,
                            },
                            'group': {
                                'r': True,
                                'w': False,
                                'x': False,
                            },
                            'other': {
                                'r': True,
                                'w': False,
                                'x': False,
                            },
                        },
                        'contents': [
                            {
                                'type': 'file',
                                'basename': 'file-test.txt',
                                'owner': 'alice',
                                'group': 'alice',
                                'perms': {
                                    'owner': {
                                        'r': True,
                                        'w': True,
                                        'x': False,
                                    },
                                    'group': {
                                        'r': True,
                                        'w': False,
                                        'x': False,
                                    },
                                    'other': {
                                        'r': True,
                                        'w': False,
                                        'x': False,
                                    },
                                },
                                'content': 'INI FILE TEST USER ALICE :)',
                            },
                        ],
                    },
                    {
                        'type': 'dir',
                        'basename': 'user',
                        'owner': 'user',
                        'group': 'user',
                        'perms': {
                            'owner': {
                                'r': True,
                                'w': True,
                                'x': False,
                            },
                            'group': {
                                'r': True,
                                'w': False,
                                'x': False,
                            },
                            'other': {
                                'r': True,
                                'w': False,
                                'x': False,
                            },
                        },
                        'contents': [
                            {
                                'type': 'file',
                                'basename': 'file-test.txt',
                                'owner': 'user',
                                'group': 'user',
                                'perms': {
                                    'owner': {
                                        'r': True,
                                        'w': True,
                                        'x': False,
                                    },
                                    'group': {
                                        'r': True,
                                        'w': False,
                                        'x': False,
                                    },
                                    'other': {
                                        'r': True,
                                        'w': False,
                                        'x': False,
                                    },
                                },
                                'content': 'INI FILE TEST USER "USER" :)',
                            },
                        ],
                    },
                ],
            }
        ],
    },
    'storage_2': {
        'product_code': 'STG:SEAGATE:BARRACUDA',
        'type': 'HARD_DISK_SATA',
        'speed': 500,  # In MB/s
        'cap': 4_000_000,  # In MB
        'contents': [
            {
                'type': 'dir',
                'basename': 'Data',
                'owner': 'alice',
                'group': 'alice',
                'perms': {
                    'owner': {
                        'r': True,
                        'w': True,
                        'x': False,
                    },
                    'group': {
                        'r': True,
                        'w': True,
                        'x': False,
                    },
                    'other': {
                        'r': True,
                        'w': True,
                        'x': False,
                    },
                },
                'contents': [
                    {
                        'type': 'file',
                        'basename': 'Shared-Data.txt',
                        'owner': 'alice',
                        'group': 'alice',
                        'perms': {
                            'owner': {
                                'r': True,
                                'w': True,
                                'x': False,
                            },
                            'group': {
                                'r': True,
                                'w': True,
                                'x': False,
                            },
                            'other': {
                                'r': True,
                                'w': True,
                                'x': False,
                            },
                        },
                        'content': 'File ini dapat dibaca dan diedit oleh siapa saja.',
                    },
                ],
            },
            {
                'type': 'file',
                'basename': 'ALICE-SECRET.txt',
                'owner': 'alice',
                'group': 'alice',
                'perms': {
                    'owner': {
                        'r': True,
                        'w': True,
                        'x': False,
                    },
                    'group': {
                        'r': False,
                        'w': False,
                        'x': False,
                    },
                    'other': {
                        'r': False,
                        'w': False,
                        'x': False,
                    },
                },
                'content': 'File ini hanya dapat dibaca oleh user alice.',
            },
        ],
    },
}

detected_hardware: dict | None = None
pwd: str | None = None


def boot():
    global detected_hardware

    def post():
        print('POST: checking CPU')

        if hardware.get('cpu') is None:
            print('CPU not detected')
            exit(1)

        if hardware.get('cpu').get('arch') not in {'x86', 'x86_64', 'x64'}:
            print('Unrecognized CPU Arch')
            exit(1)

        if hardware.get('cpu').get('bit') not in {64, 32}:
            print('Unrecognized CPU Bit-ness')
            exit(1)

        print('POST: checking CPU finish')
        print('POST: checking memory')

        valid_memory = 0

        for slot in (1, 3, 2, 4):
            if hardware.get(f'memory_{slot}') is None:
                if valid_memory == 0:
                    print(f'Memory {slot} not detected')
                    exit(1)
                else:
                    continue

            if hardware.get(f'memory_{slot}').get('version') != 'DDR4':
                print('Unrecognized memory version')
                exit(1)

            if not isinstance(hardware.get(f'memory_{slot}').get('speed'), int):
                print(f'Unrecognized memory {slot} speed')
                exit(1)

            if not isinstance(hardware.get(f'memory_{slot}').get('cap'), int):
                print(f'Unrecognized memory {slot} capacity')
                exit(1)

            valid_memory += 1

        print('POST: checking memory finish')

        for prefix, total_slot, name in (
                ('pcie', 2, 'PCI-E'),
                ('storage', 2, 'storage'),
        ):
            print(f'POST: checking {name}')

            for slot in range(1, total_slot + 1):
                if hardware.get(f'{prefix}_{slot}') is None:
                    continue

                if hardware.get(f'{prefix}_{slot}').get('type') is None:
                    print(f'Unrecognized {name} {slot} type')
                    exit(1)

            print(f'POST: checking {name} finish')

    def detect_hardware():
        valid_memory_versions = {'DDR4', 'DDR5'}
        valid_pcie_types = {'GPU'}
        valid_storage_types = {'HARD_DISK_SATA', 'SSD_NVME'}

        result = {
            'cpu': hardware.get('cpu'),
            'memory': [],
            'total_memory': 0,
            'gpu': [],
            'pcie': [],
            'storage': [],
            'total_storage': 0,
        }

        lowest_memory_speed: int | None = None

        for slot in hardware.keys():
            if slot.startswith('memory'):
                component = hardware.get(slot)

                if component is None:
                    continue

                if component.get('version') not in valid_memory_versions:
                    print(f'Unsupported memory version on {slot}. Skipping')
                    continue

                if lowest_memory_speed is None or component.get('speed') < lowest_memory_speed:
                    lowest_memory_speed = component.get('speed')

                result.update({'total_memory': result.get('total_memory') + component.get('cap')})
                result.get('memory').append(component)

            elif slot.startswith('pcie'):
                component = hardware.get(slot)

                if component is None:
                    continue

                if component.get('type') not in valid_pcie_types:
                    print(f'Unsupported PCI-E on {slot}. Skipping')
                    continue

                if component.get('type') == 'GPU':
                    result.get('gpu').append(component)
                    continue

                result.get('pcie').append(component)

            elif slot.startswith('storage'):
                component = hardware.get(slot)

                if component is None:
                    continue

                if component.get('type') not in valid_storage_types:
                    print(f'Unsupported storage on {slot}. Skipping')
                    continue

                result.update({'total_storage': result.get('total_storage') + component.get('cap')})
                result.get('storage').append(component)

        if len(result.get('memory')) == 0:
            print('Failed to start, no memory found')
            exit(1)

        for memory in result.get('memory'):
            if memory.get('speed') != lowest_memory_speed:
                print(f'Changing memory speed to {lowest_memory_speed} Mhz')
                memory.update({'speed': lowest_memory_speed})

        return result

    def load():
        bootloader_size = 500_000

        for idx, storage in enumerate(detected_hardware.get('storage')):
            if storage.get('cap') < bootloader_size:
                continue

            if len(storage.get('contents')) < 1:
                continue

            if storage.get('contents')[0].get('type') == 'boot':
                print(f'Using bootloader on storage {idx + 1}')

                storage.update({'root': True})

                return

        print('No bootloader found')
        exit(1)

    def mount():
        mount_point: dict | None = None
        mounts = []

        def resolve_mount_point(_storage: dict):
            _mount_point = None

            for item in _storage.get('contents'):
                if item.get('basename') != 'mnt':
                    continue

                if item.get('type') != 'dir':
                    print('Unable to mount storage. Could not create mount point')

                _mount_point = item
                break

            if _mount_point is None:
                _mount_point = {
                    'type': 'dir',
                    'basename': 'mnt',
                    'owner': 'root',
                    'group': 'root',
                    'perms': {
                        'owner': {
                            'r': True,
                            'w': True,
                            'x': False,
                        },
                        'group': {
                            'r': True,
                            'w': False,
                            'x': False,
                        },
                        'other': {
                            'r': True,
                            'w': False,
                            'x': False,
                        },
                    },
                    'contents': [],
                }

                _storage.get('contents').append(_mount_point)

            return _mount_point

        def mount_queued():
            for idx, _mount in enumerate(mounts):
                mount_point.get('contents').append({
                    'type': 'dir',
                    'basename': f'stg{idx + 1}',
                    'owner': 'root',
                    'group': 'root',
                    'perms': {
                        'owner': {
                            'r': True,
                            'w': True,
                            'x': False,
                        },
                        'group': {
                            'r': True,
                            'w': True,
                            'x': False,
                        },
                        'other': {
                            'r': True,
                            'w': True,
                            'x': False,
                        },
                    },
                    'contents': _mount.get('contents'),
                })

        for storage in detected_hardware.get('storage'):
            if storage.get('root'):
                mount_point = resolve_mount_point(storage)
                mount_queued()
            else:
                mounts.append(storage)

        if mount_point is None:
            print('Unable to mount storage. Mount point not found')
            exit(1)

        mount_queued()

    def show():
        def product_name(code: str):
            return product.get(code) if product.get(code) is not None else 'Unknown'

        print(
            f'CPU: {product_name(detected_hardware.get('cpu').get('product_code'))}'
            f' ({detected_hardware.get('cpu').get('arch')} {detected_hardware.get('cpu').get('bit')}bit)'
            f' - ({detected_hardware.get('cpu').get('speed')}) MHz'
        )

        prefix = '\n  - ' if len(detected_hardware.get('gpu')) != 1 else ''
        gpu = ''

        for component in detected_hardware.get('gpu'):
            gpu += f'{prefix}{product_name(component.get('product_code'))} - {component.get('speed')} MHz'

        print(f'GPU: {gpu}')

        prefix = '\n  - ' if len(detected_hardware.get('memory')) != 1 else ''
        memory = ''

        for component in detected_hardware.get('memory'):
            memory += (
                f'{prefix}{product_name(component.get('product_code'))}'
                f' ({component.get('cap')} MB)'
                f' - {component.get('speed')} MHz'
            )

        print(f'Total memory: {detected_hardware.get('total_memory')} MB')
        print(f'Memory: {memory}')

        prefix = '\n  - ' if len(detected_hardware.get('storage')) != 1 else ''
        storage = ''

        for component in detected_hardware.get('storage'):
            storage += (
                f'{prefix}{product_name(component.get('product_code'))}'
                f' ({component.get('cap')} MB)'
                f' - {component.get('speed')} MB/s'
            )

        print(f'Total storage: {detected_hardware.get('total_storage')} MB')
        print(f'Storage: {storage}')

    post()
    print()
    detected_hardware = detect_hardware()
    print()
    load()
    mount()
    print()
    show()


def auth() -> dict:
    users = (
        {
            'uid': 1,
            'user': 'root',
            'password': 'root',
            'groups': ['root'],
            'home': '/',
        },
        {
            'uid': 1000,
            'user': 'alice',
            'password': 'alice',
            'groups': ['alice', 'wheel'],
            'home': '/home/alice/',
        },
        {
            'uid': 1001,
            'user': 'user',
            'password': 'user',
            'groups': ['user'],
            'home': '/home/user/',
        },
    )

    max_attempt = 3

    for attempt in range(max_attempt):
        user = input('User: ')
        password = input('Password: ')

        for _user in users:
            if _user.get('user') == user and _user.get('password') == password:
                return _user

        print('User dan password tidak cocok. Silakan coba lagi')

    print('Gagal autentikasi user')
    exit(1)


def shell(user: dict):
    def greet():
        print(f'Selamat datang, {user.get('user')}!')

        prefix = '\n  - ' if len(user.get('groups')) != 1 else ''
        group = ''

        for user_group in user.get('groups'):
            group += prefix + user_group

        print(f'Grup: {group}')

    def root_storage():
        for storage in detected_hardware.get('storage'):
            if storage.get('root'):
                return storage

        print('Unable to find root storage')
        exit(1)

    def pwd_item():
        global pwd

        path = pwd.strip('/')
        paths = [_slice for _slice in path.split('/') if _slice != '']
        curr = root_storage()

        for folder in paths:
            found = False

            for item in curr.get('contents'):
                if item.get('type') != 'dir':
                    continue

                if item.get('basename') == folder:
                    found = True
                    curr = item
                    break

            if not found:
                print('Failed to get PWD item')
                exit(1)

        return curr

    def pwd_contents():
        return pwd_item().get('contents')

    def cd(path: str):
        global pwd

        pwd_before = pwd
        is_absolute = path.startswith('/')

        if is_absolute:
            pwd = '/'

        if path == '/':
            return

        path = path.strip('/')
        paths = path.split('/')

        for folder in paths:
            if folder == '.':
                continue

            found = False

            if folder == '..':
                if pwd != '/':
                    pwd, _ = pwd.removesuffix('/').rsplit('/', 1)
                    pwd += '/'
                    found = True
            else:
                for item in pwd_contents():
                    if item.get('type') != 'dir':
                        continue

                    if item.get('basename') == folder:
                        found = True
                        pwd += f'{folder}/'
                        break

            if not found:
                print(f'Failed to change directory to {path}')
                pwd = pwd_before
                return

    def ensure_perm(item: dict, perm: str) -> bool:
        if user.get('uid') == 1:
            return True

        if item.get('perms').get('other').get(perm):
            return True

        if item.get('group') in user.get('groups') and item.get('perms').get('group').get(perm):
            return True

        if item.get('owner') == user.get('user') and item.get('perms').get('owner').get(perm):
            return True

        return False

    def home():
        cd(user.get('home'))

    def prompt():
        global pwd

        print(pwd.replace(user.get('home'), '~/', 1), '$' if user.get('uid') != 1 else '#', end=' ')

    def handle_input():
        def split_exec(_input: str):
            if _input.find(' ') == -1:
                return _input, ''

            return _input.split(' ', 1)

        def _cd(_input: str):
            _, path = split_exec(_input)

            if path == '':
                path = user.get('home')

            cd(path)

        def _exit(_input: str):
            print('Shutting down system')
            exit()

        def ls(_input: str):
            for item in pwd_contents():
                if item.get('type') == 'boot':
                    continue

                attr = 'd' if item.get('type') == 'dir' else '-'

                for perms_section in ('owner', 'group', 'other'):
                    perms = item.get('perms').get(perms_section)

                    attr += 'r' if perms.get('r') else '-'
                    attr += 'w' if perms.get('w') else '-'
                    attr += 'x' if perms.get('x') else '-'

                print(attr, item.get('basename'))

        def cat(_input: str):
            global pwd

            _, files = split_exec(_input)
            files = files.strip().split(' ')
            pwd_before = pwd

            for file in files:
                if file.find('/') != -1:
                    path, file = file.rsplit('/', 1)
                    cd(path)

                for item in pwd_contents():
                    if item.get('type') != 'file':
                        continue

                    if item.get('basename') != file:
                        continue

                    if not ensure_perm(item, 'r') and not ensure_perm(item, 'w'):
                        print(f'Unable to open {file}. Insufficient permission')
                        continue

                    print(item.get('content'))

                pwd = pwd_before

        def _pwd(_input: str):
            global pwd

            print(pwd)

        def rm(_input: str):
            global pwd

            is_recursive = _input.find('-r') != -1

            for param in {'-r'}:
                _input.replace(param, '')

            _, files = split_exec(_input)
            files = files.strip().split(' ')
            pwd_before = pwd

            for file in files:
                if file.find('/') != -1:
                    path, file = file.rsplit('/', 1)
                    cd(path)

                delete = []

                for idx, item in enumerate(pwd_contents()):
                    if item.get('basename') != file:
                        continue

                    if item.get('type') == 'dir' and not is_recursive:
                        print('Use parameter -r to confirm delete folder')
                        continue

                    if not ensure_perm(item, 'w'):
                        print(f'Unable to delete {file}. Insufficient permission')
                        continue

                    delete.insert(0, idx)

                for idx in delete:
                    pwd_contents().pop(idx)
                    print(f'Success delete {file}')

                pwd = pwd_before

        def mkdir(_input: str):
            if not ensure_perm(pwd_item(), 'w'):
                print(f'Unable to create folder in this directory. Insufficient permission')
                return

            _, files = split_exec(_input)
            files = files.strip().split(' ')

            for file in files:
                if file.find('/') != -1 or file.find(' ') != -1:
                    print(f'Unable to create folder {file}. Invalid character')
                    continue

                pwd_contents().append({
                    'type': 'dir',
                    'basename': file,
                    'owner': user.get('user'),
                    'group': user.get('groups')[0],
                    'perms': {
                        'owner': {
                            'r': True,
                            'w': True,
                            'x': False,
                        },
                        'group': {
                            'r': True,
                            'w': False,
                            'x': False,
                        },
                        'other': {
                            'r': True,
                            'w': False,
                            'x': False,
                        },
                    },
                    'contents': [],
                })

        def touch(_input: str):
            if not ensure_perm(pwd_item(), 'w'):
                print(f'Unable to create file in this directory. Insufficient permission')
                return

            _, files = split_exec(_input)
            files = files.strip().split(' ')

            for file in files:
                if file.find('/') != -1 or file.find(' ') != -1:
                    print(f'Unable to create file {file}. Invalid character')
                    continue

                pwd_contents().append({
                    'type': 'file',
                    'basename': file,
                    'owner': user.get('user'),
                    'group': user.get('groups')[0],
                    'perms': {
                        'owner': {
                            'r': True,
                            'w': True,
                            'x': False,
                        },
                        'group': {
                            'r': True,
                            'w': False,
                            'x': False,
                        },
                        'other': {
                            'r': True,
                            'w': False,
                            'x': False,
                        },
                    },
                    'content': '',
                })

        def append(_input: str):
            global pwd

            _, _args = split_exec(_input)
            file, content = _args.strip().split(' ', 1)
            pwd_before = pwd

            if file.find('/') != -1:
                path, file = file.rsplit('/', 1)
                cd(path)

            found = False

            for item in pwd_contents():
                if item.get('type') != 'file':
                    continue

                if item.get('basename') != file:
                    continue

                if not ensure_perm(item, 'w'):
                    print(f'Unable to modify {file}. Insufficient permission')
                    continue

                if item.get('content') == '':
                    item.update({'content': content})
                else:
                    item.update({'content': item.get('content') + f'\n{content}'})

                found = True
                break

            pwd = pwd_before

            if not found:
                print(f'Unable to modify {file}. File not found')

        def _help(_input: str):
            print(
                """Commands:
  - help     -> Digunakan untuk menampilkan bantuan
  - pwd      -> Digunakan untuk melihat path direktori saat ini
  - cd       -> Digunakan untuk berpindah direktori
  - ls       -> Digunakan list folder dan file di direktori saat ini
  - cat      -> Digunakan untuk melihat isi file
  - rm       -> Digunakan untuk menghapus folder dan file
  - mkdir    -> Digunakan untuk membuat folder di direktori saat ini
  - touch    -> Digunakan untuk membuat file di direktori saat ini
  - append   -> Digunakan untuk menambah isi file
  - exit     -> Digunakan shutdown sistem"""
            )

        command = {
            'cd': _cd,
            'exit': _exit,
            'ls': ls,
            'cat': cat,
            'pwd': _pwd,
            'rm': rm,
            'mkdir': mkdir,
            'touch': touch,
            'append': append,
            'help': _help,
        }

        _input = input()
        _exec, args = split_exec(_input)

        if command.get(_exec) is not None:
            command.get(_exec)(_input)
        else:
            print(f'Unable to find command {_exec}')

    greet()
    home()
    print()

    while True:
        prompt()
        handle_input()


def main():
    boot()
    print()
    user = auth()
    print()
    shell(user)


if __name__ == '__main__':
    main()
